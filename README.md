# Bucket Sampling for Earth System Data Cubes
![Title Image](title-image.png)

The concept of the Earth System Data Cube (Mahecha et al. 2020) rapidly turned into a
popular tool in Earth System Sciences during the last years as it tremendously facilitates data
visualization and (interoperable) data handling, including preprocessing or statistical analyses.
The original data sets are transformed in space and time to fit to the common grid of the Data
Cube which consists of three dimensions: longitude, latitude and time, and further holds a set of
variables that are mapped into this spatio-temporal system. Data Cubes are typically chunked,
meaning they consist of a set of smaller cubes (chunks) which together build what we call the
Earth System Data Cube (ESDC). The ESDC concept allows to treat multiple remotely sensed
spatio-temporal data streams as a singular one and therefore enables to interact with a wide
range of data.
A parallel development is the growing need for the application of Machine Learning methods to
Earth System Sciences data as most parts of the Earth system are continuously monitored by
sensors and Machine Learning is able to cope with both the volume of data and the
heterogeneous data characteristics. For instance, satellites monitor the atmosphere, land, and
ocean with unprecedented accuracy. Ideally, classical operations on the ESDC could be
extended by Machine Learning applications in order to sustain interoperability. However, there
is a conflict between the nature of remotely-sensed data, the structure of the ESDC and the
requirements for meaningful Machine Learning applications which need to be addressed:
1. Sampling the Earth naturally leads to an uneven distribution of data points as a result of
its spherical shape. This phenomenon is reinforced by data gaps due to e.g., satellite
trajectories or cloud cover. Hence, there is no uniform data distribution across the
chunks of the ESDC provided.
2. Remotely sensed data tends to be auto-correlated within (neighboring) chunks as data
points which are in close spatio-temporal vicinity are naturally characterized by a low
variance.
Therefore, it is mandatory to enable Machine Learning that respects the basic principles of geodata
way beyond naive applications of Machine Learning in the Earth system context. More
specific, sophisticated and efficient sampling strategies need to be developed. We here propose
the implementation of a novel bucket sampling strategy together with a set of helper tools,
which account for challenges 1. and 2. and enable efficient and meaningful ML applications on
Earth System Data Cubes (Fig. 1).
Bucket sampling consists of five major steps: preprocessing, data split, bucketing, model
training and finally, building the combined bucket model. Preprocessing involves the standard
techniques such as cleaning, filtering, gap filling, etc. The data split step enables users to split
the data according to their specific needs (e.g., random, blocks, 80/20) into train and test data.
During bucketing, we will account for the uneven data distribution across the chunks of the
ESDC by implementing tools such as oversampling, undersampling or data augmentation. This
step ensures that each bucket contains the same amount of representative data from each
chunk. Once the data is resampled and balanced within the buckets, the ML models can be
trained independently in a distributed manner and combined in a final step. 

## Abstract
Sampling remotely sensed time series data for training a Machine Learning model is not trivial
due to their inherent characteristics, such as their uneven data distribution in space and time,
auto-correlation effects for data points in close spatio-temporal vicinity and their high data
volume which needs to be handled in an efficient manner. Based on previously developed basic
Machine Learning Tools for remotely sensed data in the form of an Earth System Data Cubes
(ESDC), we will introduce and implement a new bucket sampling strategy that accounts for the
special characteristics of geo-spatial data. We will develop and provide an open-source
software (wrapper).

## Outcomes and Trends
- open-source bucket sampling wrapper published on Git-Hub
- Jupyter Notebook showcasing wrapper usage based on a scientific use case
## Funding

This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).
