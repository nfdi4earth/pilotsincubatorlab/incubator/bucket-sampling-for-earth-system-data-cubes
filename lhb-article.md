---
name: Bucket Sampling for Earth System Data Cubes
#toDo: set the article name to your project name
# The title of the article.

description: 
#toDO: copy your abstract
Sampling remotely sensed time series data for training a Machine Learning model is not trivial
due to their inherent characteristics, such as their uneven data distribution in space and time,
auto-correlation effects for data points in close spatio-temporal vicinity and their high data
volume which needs to be handled in an efficient manner. Based on previously developed basic
Machine Learning Tools for remotely sensed data in the form of an Earth System Data Cubes
(ESDC), we will introduce and implement a new bucket sampling strategy that accounts for the
special characteristics of geo-spatial data. We will develop and provide an open-source
software (wrapper).
# A brief summary of the article, max. 300 characters incl. spaces.

author: 
  - name: Josefine Umlauft
    orcidid: https://orcid.org/0000-0002-6108-432X
  - name: Julia Peters
#toDo: replace the authors with the project coworkers, the orcid is optional for all further author but mandatory for the first author.
# The author(s) of the article. The name must be given, the ORCID should be given. As many authors as necessary can be listed. 


language: ENG
# The language of the article. Accepted values are:  
# ENG for English
# DEU for German

collection: 
  - N4E_Incubators.md
# The filenames of the NFDI4Earth Living Handbook collection(s), the article belongs to, as they appear in https://git.rwth-aachen.de/nfdi4earth/livinghandbook/livinghandbook/-/tree/main/docs. 
# Alternatively, the titles of the collections. The editor will replace it with the collections' file names. 

subtype: article
# The type of the article. Acceptable terms are: 
# article: The most generic category. Should only be used if none of the others fit. 
# best_practice: A procedure of how a defined problem described in the article can be solved which is a (de-facto) standard or widely accepted in the community
# workflow: A step-by-step description of a procedure
# technical_note: A short article presenting a new software tool, method, technology. 
# recommended_article: a short review of a publication.
# discussion/review: An article that summarises/compares or (critically) discusses e.g., the state of understanding, different approaches or methodologies of a topic. 
# user_guide: Manuals on how to use e.g., software products. 
# collection: A NFDI4Earth Living Handbook collection article.
# FAQ: Can only used by the NFDI4Earth User Support Network, which define the FAQs. 

subject: 
  - dfgfo:314-01
  - unesco:concept3052
  - unesco:mt2.35
#toDO: adapt the subject to your project subject
# A list of controlled terms describing the subject of the article, given as their identifiers. Acceptable are any terms from: 
# DFG subject ontology: https://terminology.tib.eu/ts/ontologies/dfgfo/terms
# UNESCO thesaurus: https://vocabularies.unesco.org/browser/thesaurus/en/

keywords:
  - Data Science
  - Machine Learning
  - Data Cubes
  - Sampling
#toDo: adapt the kewords
# A list of terms to describe the article's topic more precisely. 

target_role: 
  - data collector
  - data owner
  - data provider
  - service provider
# A list of the main roles of persons within research data management that is addressed by the article. 
# data collector: The person responsible for collecting, generating, re-using or contributing to research data and documents contexts and processes that relates to data handling. 
# data owner: Typically the principal researcher of the project or research group leader who is responsible for establishing who has access to the data, and reducing or mitigating the risk of any mishaps; e.g. improper storage facility, non-compliance with specific guidelines. 
# data depositor: Either the data creator or data owner and is the person who will perform the deposit of the data to a data preservation facility, like a repository or data archive. They perform activities such as uploading data after it has been generated, making it suitable for archiving, creating metadata and data documentation and submitting the data to the facility, so it can be preserved for the long-term. 
# data user: Someone who is interested in accessing and interacting with data for verification or (re)using data to evaluate, analyse, model and generally handle then to create insights and knowledge. access to research data for re-use or verification. 
# data curator: A person who ensures availability and reliability of data. They review and harmonise metadata of datasets submitted for storage and ensure that they are generally compliant with relevant policies before publication. They further monitor access to the data, support data owners in fulfilling data access requests and possess deeper technical knowledge about the backend of the repository or storage system. 
# data librarian: An information professional responsible for developing technical expertise on practical solutions of data management, archiving and dissemination and on data mining and visualization. 
# data steward: A supporter who advises researchers (or otherwise involved) persons on data management, privacy and information security aspects and can understand the meaning and applications of the data. They support: data creators on quality assurance, storage, policy compliance; data depositors on data selection, metadata and data documentation creation, preserving and publishing; data custodians on data curation, access controls, FAIR data management 
# data advocate: A person promoting the proper use of research data and technology and providing assistance or guidance in doing so. 
# data provider: A person or organization that stores and provides datasets to others.
# service provider: An organization that provides services related to the processing and management of data, such as, high-performance computing facilities or web-based maps.
# research software engineer: A person working with researchers to gain an understanding of the problems they face, and then develops, maintains and extends software to provide the answers. 
# policy maker: In research data management, policy makers strive to design effective, technology-neutral, forward-looking and coherent data governance policies across sectors.
# general public: All persons that do not belong to any of the other groups. 

identifier: 
# If the article has already a unique identifier such as an DOI, provide it here as URL (e.g., https://doi.org/10.1001/12345). 

version: 0.1
# The version of the article; will be updated by the editor. 

#toDo: convert your project description to markdown by copying the text and adapting headings, links and reference. References could be stored in the bib.bib file in bibtex format and can cites as discripted in th LHB (https://nfdi4earth.pages.rwth-aachen.de/livinghandbook/livinghandbook/#LHB_author-guidelines/#references)
---
# Bucket Sampling for Earth System Data Cubes
![Title Image](title-image.png)



## Abstract
Sampling remotely sensed time series data for training a Machine Learning model is not trivial
due to their inherent characteristics, such as their uneven data distribution in space and time,
auto-correlation effects for data points in close spatio-temporal vicinity and their high data
volume which needs to be handled in an efficient manner. Based on previously developed basic
Machine Learning Tools for remotely sensed data in the form of an Earth System Data Cubes
(ESDC), we will introduce and implement a new bucket sampling strategy that accounts for the
special characteristics of geo-spatial data. We will develop and provide an open-source
software (wrapper).

## Outcomes and Trends
As the project is scheduled to start in spring 2023, there are no outcomes yet, the following deliverables are planned:
- open-source bucket sampling wrapper published on Git-Hub
- Jupyter Notebook showcasing wrapper usage based on a scientific use case

## Resources
* Code repository ([GitLab](https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/bucket-sampling-for-earth-system-data-cubes ))
